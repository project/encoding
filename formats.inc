<?php

$formats = array(

'output' => //(required) – output format. Specify format of encoded file.
  array('flv', 'fl9', 'wmv', '3gp', 'mp4', 'm4v', 'ipod', 'iphone', 'ipad', 'appletv', 'psp', 'zune', 'mp3', 'wma', 'm4a', 'thumbnail', 'image', 'mpeg2', 'iphone_stream'),

'size' => // [optional] - Video frame size.
  array(
    '3gp' => array('128x96', '176x144', '352x288', '704x576', '1408x1152'),
    'appletv' => array('710x480'),
    'zune' => array('320x180', '320x240'),
    'vp6' => array('special' => 'return encoding_validate_size16($v);'),
  ),


'bitrate' => // [optional] - Video bitrate.
  array('default' => array('special' => 'return encoding_validate_bitrate($v);')), //Nk - where N is any non-zero integer.

'framerate' => // [optional] - Frame rate.
  array('default' => array('special' => 'return encoding_validate_framerate($v);')), //any non-zero integer or N/M where N and M are non-zero integers.


'video_codec' => // [optional] - Video codec.
  array(
    'flv' => array('flv', 'libx264', 'vp6'),
    'fl9' => array('libx264'),
    'wmv' => array('wmv2', 'msmpeg4'),
    'zune' => array('wmv2', 'msmpeg4'),
    '3gp' => array('h263', 'libx264'),
    'm4v' => array('mpeg4'),
    'mp4' => array('mpeg4', 'libx264'),
    'ipod' => array('mpeg4', 'libx264'),
    'iphone' => array('mpeg4', 'libx264'),
    'ipad' => array('mpeg4', 'libx264'),
    'appletv' => array('mpeg4', 'libx264'),
    'psp' => array('mpeg4', 'libx264'),
    'mp3' => array(),
    'wma' => array(),
    'mpeg2' => array('mpeg2video'),
  ),


'audio_bitrate' => // [optional] - Audio bitrate.
//Nk - where N is any non-zero integer
  array(
   '3gp' => array('4.75k', '5.15k', '5.9k', '6.7k', '7.4k', '7.95k', '10.2k', '12.2k'),
   'default' => array('32k', '40k', '48k', '56k', '64k', '80k', '96k', '112k', '128k', '144k', '160k', '192k', '224k', '256k', '320k'),
  ),

'audio_sample_rate' => // [optional] - Audio sampling frequency (Hz).
//any non-zero integer.
  array(
    '3gp' => array('8000'),
    'flv' => array('11025', '22050', '44100'),
    'mp3' => array('11025', '22050', '44100'),
    'wmv' => array('11025', '22050', '32000', '44100', '48000'),
    'wma' => array('11025', '22050', '32000', '44100', '48000'),
    'zune' => array('11025', '22050', '32000', '44100', '48000'),
    'mpeg2' => array('44100', '48000'),
  ),


'audio_codec' => // [optional] - Audio codec.
  array(
    'mp3' => array('libmp3lame'),
    'm4a' => array('libfaac'),
    'flv' => array('libmp3lame', 'libfaac'),
    'fl9' => array('libfaac'),
    'mp4' => array('libfaac'),
    'm4v' => array('libfaac'),
    'ipod' => array('libfaac'),
    'iphone' => array('libfaac'),
    'ipad' => array('libfaac'),
    'appletv' => array('libfaac'),
    'psp' => array('libfaac'),
    'wmv' => array('wmav2', 'libmp3lame'),
    'wma'  => array('wmav2', 'libmp3lame'),
    'zune' => array('wmav2', 'libmp3lame'),
    '3gp' => array('libamr_nb'),
    'mpeg2' => array('pcm_s16be', 'pcm_s16le'),
  ),

'audio_channels_number' => // [optional] - Number of audio channels.
//any non-zero integer.
  array(
    '3gp' => array('1'),
    'default' => array('special' => 'return is_int($v) && $v != 0;'),
  ),

'audio_volume' => // [optional] - Audio volume level, in percent.
  array('default' => array('special' => 'return encoding_validate_positive_int($v);')), //non-negative integer

'two_pass' => // [optional] - Whether to use 2-pass encoding
  array('default' => array('yes', 'no')),

'cbr' => // [optional] - Whether to use CBR (Constant bitrate)
  array('default' => array('yes', 'no')),

'maxrate' => // [optional] - Maximum allowed video bitrate.
  array('default' => array('special' => 'return encoding_validate_bitrate($v);')),

'minrate' => // [optional] - Minimum allowed video bitrate.
  array('default' => array('special' => 'return encoding_validate_bitrate($v);')),

'bufsize' => // [optional] - Rate control buffer size (bits).
  array('default' => array('special' => 'return encoding_validate_bitrate($v);')),

'keyframe' => // [optional] - Keyframe period', 'in frames.
  array('default' => array('special' => 'return encoding_validate_positive_int($v);')),

'start' => // [optional] - Start encoding from (sec).
  array('default' => array('special' => 'return encoding_validate_positive_int($v);')),

'duration' => // [optional] - Duration (sec).
  array('default' => array('special' => 'return encoding_validate_positive_int($v);')),

'rc_init_occpancy' => // [optional] - Initial rate control buffer occupancy (bits).
  array('default' => array('special' => 'return encoding_validate_bitrate($v);')),

'deinterlacing' => // [optional] - Whether to use de-interlacing
  array('default' => array('yes', 'no')),

'crop_top' => // [optional] - Top crop band size (in pixels).
  array('default' => array('special' => 'return encoding_validate_even_int($v);')),

'crop_left' => // [optional] - Left crop band size (in pixels).
  array('default' => array('special' => 'return encoding_validate_even_int($v);')),

'crop_right' => // [optional] - Right crop band size (in pixels).
  array('default' => array('special' => 'return encoding_validate_even_int($v);')),

'crop_bottom' => // [optional] - Bottom crop band size (in pixels).
  array('default' => array('special' => 'return encoding_validate_even_int($v);')),

'keep_aspect_ratio' => // [optional] - Whether to keep width to height ratio of the original frame size
  array('default' => array('yes', 'no')),


'add_meta' => // [optional] - 'flv' only. Whether to add meta data to the file.
  array(
        'default' => array(),
        'flv' => array('yes', 'no'),
       ),

'rotate' =>// [optional] - video files only. Rotate video picture.
  array(
    'def', //don't change anything. Video will be rotated according to 'Rotation' meta data parameter', 'if it exists
    '0',   //don't rotate and ignore 'Rotation' meta data parameter
    '90',  //rotate by 90 degree CW and ignore 'Rotation' meta data parameter
    '180', //rotate by 180 degree and ignore 'Rotation' meta data parameter
    '270', //rotate by 270 degree CW and ignore 'Rotation' meta data parameter
  ),

);
